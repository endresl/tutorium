package utils;//package utils;

import java.util.*;

public class CmdParser {

    private static Set<String> optional_parameters = new HashSet<>();
    private static String mandatoryParameter = "-mandatory";

    public static void main(String[] args) {

        HashMap<String, String> map = new HashMap<>();

        optional_parameters.add("-int");
        optional_parameters.add("-double");
        optional_parameters.add("-file");
        optional_parameters.add("-value");


        boolean mandaParameter = false;
        int numberofinputs = args.length;

        for (int i = 0; i < numberofinputs; i = i + 2) {
            if (mandatoryParameter.equals(args[i])) {
                mandaParameter = true;
            }
        }
        if (mandaParameter) {
            if (numberofinputs % 2 == 1) {
                System.err.println("there is something wrong with your input");
            } else {
                try {


                    for (int i = 0; i < numberofinputs; i = i + 2) {

                        String key = args[i];
                        String value = args[i + 1];


                        if (optional_parameters.contains(key) | key.equals(mandatoryParameter))
                            map.put(key, value);
                        else
                            System.err.println("wrong parameter: " + key);

                    }
                } catch (ArrayIndexOutOfBoundsException e) {
                    System.err.println("something went wrong with the input");

                }
            }
        } else {
            System.err.println("mandatory parameter missing");
        }

        for (String key2 : map.keySet()) {
            System.out.println(key2 + ":" + map.get(key2));
        }

    }


}