package utils;

import jdk.nashorn.api.tree.FunctionCallTree;

import java.io.*;
import java.util.*;
import java.util.function.Function;
import java.util.stream.Collectors;

public class ReadFile {

    private static List<String> lines;
    private static List<String[]> lines_splitted;


    public static void main(String[] args) throws IOException {

        //test methods
        //readFile(new File("TestText.txt"));
        //readTSV(new File("TSVTestFile.tsv"));
        //readTSVintoHashmap(new File("TSVTestFile.tsv"), 1, 2, ",");
        Function<String, String> StringtoString = (String s) -> s;
        Function<String, Integer> StringtoInt = (String s) -> Integer.parseInt(s);
        readanytsv(new File("TSVTestFile.tsv"), StringtoString, StringtoInt, 1, 2);
    }


    public static List<String> readFile(File file) {
        lines = new ArrayList<>();
        String line;

        try (BufferedReader reader = new BufferedReader(new FileReader(file))) {
            while ((line = reader.readLine()) != null) {
                lines.add(line);
            }
        } catch (IOException e) {
            System.err.println("something went wrong while processing the input file " + file.getName());
        }

        //collection.forEach(System.out::println);//Test: Gesamte Collection auf Konsole ausgeben
        return lines;

    }

    public static List<String[]> readTSV(File file, String delimiter) {

        lines_splitted = new ArrayList<>();
        String line;

        try (BufferedReader reader = new BufferedReader(new FileReader(file))) {
            while ((line = reader.readLine()) != null) {
                String[] parts = line.split(delimiter);
                lines_splitted.add(parts);
            }
        } catch (IOException e) {
            System.err.println("something went wrong while processing the input file " + file.getName());
        }
        //collection.forEach(System.out::println);//Prints collection
        return lines_splitted;
    }

    public static List<String[]> readTSV(File file) throws IOException {
        return readTSV(file, "\t");
    }

    public static HashMap<String, String> readTSVintoHashmap(File file, int keyInt, int valueInt, String delimiter) {

        HashMap<String, String> map = new HashMap<>();
        String line;
        String key, value;
        int linenumber = 0;
        try (BufferedReader reader = new BufferedReader(new FileReader(file))) {
            while ((line = reader.readLine()) != null) {
                linenumber++;
                String[] parts = line.split(delimiter);
                try {
                    key = parts[keyInt - 1];
                } catch (ArrayIndexOutOfBoundsException e) {
                    System.err.println("no column " + keyInt + " in line " + linenumber);
                    continue;
                }

                try {
                    value = parts[valueInt - 1];
                } catch (ArrayIndexOutOfBoundsException e) {
                    System.err.println("no column " + valueInt + " in line " + linenumber);
                    continue;
                }
                if (!map.containsKey(key))
                    map.put(key, value);
                else
                    System.err.println("key: '" + key + "' and value: '" + value + "' were not added. Duplicate key.");


            }
        } catch (IOException e) {
            System.err.println("something went wrong while processing the input file " + file.getName());
        }

        //prints hashMap
        //for (String key2 : map.keySet()) {
        //    System.out.println(key2 + ":" + map.get(key2));
        //}

        return map;
    }

    public static HashMap<String, String> readTSVintoHashmap(File file) throws IOException {
        return readTSVintoHashmap(file, 1, 2, "\t");
    }


    //ab hier Übungsblatt 2

    public static <A, B> HashMap<A, B> readanytsv(File file, Function<String, A> keyParser, Function<String, B> valueParser, int colkey, int colvalue) {

        HashMap<A, B> map = new HashMap<>();
        String delimiter = "\t";
        String line;
        // Function<String, ?> toINT = (String s) -> Integer.parseInt(s);

        try (BufferedReader reader = new BufferedReader(new FileReader(file))) {

            while ((line = reader.readLine()) != null) {

                String[] parts = line.split(delimiter);

                A key = keyParser.apply(parts[colkey - 1]);
                B value = valueParser.apply(parts[colvalue - 1]);

                if (!map.containsKey(key))
                    map.put(key, value);
                else
                    System.err.println("key: '" + key + "' and value: '" + value + "' were not added. Duplicate key.");
            }

        } catch (IOException e) {
            e.printStackTrace();
        }

        //print map
        for (Object h : map.keySet()) {
            System.out.println(h + ":" + map.get(h));
        }

        return map;

    }





}
