package utils;

import java.io.*;
import java.util.ArrayList;

public class Protein {


    String name;
    int length;

    public Protein(String name, int length) {
        this.name = name;
        this.length = length;
    }

    public static void main(String[] args) {
        ArrayList<Protein> proteine = readtsv(new File("C_elegans.proteinLengths.tsv"));

        StringBuilder stringBuilderForEach = new StringBuilder();


        System.out.println(proteine.stream().filter(x -> x.length > 5000).count());
        System.out.println(proteine.stream().mapToInt(x -> x.length).sum());
        System.out.println(proteine.stream().mapToInt(x -> x.length).min().getAsInt());
        System.out.println(proteine.stream().mapToInt(x -> x.length).max().getAsInt());
        System.out.println(proteine.stream().filter(x -> x.length > 5000).map(x -> x.name).reduce((a, b) -> a + b).get());
        System.out.println(proteine.parallelStream().filter(x -> x.length > 5000).map(x -> x.name).reduce((a, b) -> a + b).get());
        proteine.stream().filter(x -> x.length > 5000).forEach(a -> stringBuilderForEach.append(a.name));
        System.out.println(stringBuilderForEach.toString());
        stringBuilderForEach.setLength(0);
        proteine.parallelStream().filter(x -> x.length > 5000).forEach(a -> stringBuilderForEach.append(a.name));
        System.out.println(stringBuilderForEach.toString());
        stringBuilderForEach.setLength(0);
        proteine.stream().filter(x -> x.length > 5000).forEachOrdered(a -> stringBuilderForEach.append(a.name));
        System.out.println(stringBuilderForEach.toString());
        stringBuilderForEach.setLength(0);
        proteine.parallelStream().filter(x -> x.length > 5000).forEachOrdered(a -> stringBuilderForEach.append(a.name));
        System.out.println(stringBuilderForEach.toString());
    }


    public static ArrayList<Protein> readtsv(File file) {

        ArrayList<Protein> list = new ArrayList<>();
        String line;

        try (BufferedReader reader = new BufferedReader(new FileReader(file))) {

            while ((line = reader.readLine()) != null) {

                String[] parts = line.split("\t");
                Protein protein = new Protein(parts[0], Integer.parseInt(parts[1]));
                list.add(protein);

            }


        } catch (FileNotFoundException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }

        return list;

    }


}


