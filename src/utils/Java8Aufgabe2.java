package utils;

import java.io.IOException;
import java.util.Arrays;
import java.util.Random;

public class Java8Aufgabe2 {

    public static void main(String[] args) throws IOException {
        double[] a = new double[10];
        double[] b = new double[10];
        Random rnd = new Random();
        for (int i = 0; i < a.length; i++) {
            a[i] = rnd.nextDouble();
            b[i] = rnd.nextDouble();
        }

        System.out.println(Arrays.toString(computeAll(a, b, Java8Aufgabe2::add))); // ersetze null durch Addition
        System.out.println(Arrays.toString(computeAll(a, b, (a1, b1) -> a1 - b1))); // ersetze null durch Subtraktion
        System.out.println(Arrays.toString(computeAll(a, b, new Computer() {
            @Override
            public double compute(double a, double b) {
                return a * b;
            }
        }))); //ersetze null durch Multiplikation
        System.out.println(Arrays.toString(computeAll(a, b, new Division()))); // ersetze null durch Division

    }


    private static double[] computeAll(double[] a, double[] b, Computer computer) {
        double[] re = new double[a.length];
        for (int i = 0; i < a.length; i++) {
            re[i] = computer.compute(a[i], b[i]);
        }
        return re;
    }

    private interface Computer {
        double compute(double a, double b);
    }

    private static double add(double a, double b) {
        return a + b;
    }

    private static class Division implements Computer {
        @Override
        public double compute(double a, double b) {
            return a / b;
        }
    }

}
