#!/usr/bin/env Rscript

# ggplot2 importieren
library(ggplot2)

# Kommandozeilenparameter einlesen
args = commandArgs(trailingOnly=TRUE)

# .tsvs in data frames speichern
yeastPheromoneResponseExpr <-
  read.csv(file = args[1], sep = '\t', header = TRUE)
yeastPheromoneResponsePhospho <-
  read.table(file = args[2], sep = '\t', header = TRUE)

# data frames merged
mergeddata <- merge(yeastPheromoneResponseExpr, yeastPheromoneResponsePhospho, by.x = "YORF", by.y = "ORF")

# Funktion inplementieren, die eine Spalte(colN) aus yeastPheromoneResponseExpr mit der fc Messung aus yeastPheromoneResponsePhospho plottet und speichert.
drawPlot <- function(colN){
    tmpPlot <- ggplot(data=mergeddata,aes(x=mergeddata[,colN], y=fc))+ 
    geom_point()+
    labs(subtitle=paste0("Genexpression (nach ", substring(colnames(mergeddata)[colN],2) , ") vs Proteinexpression (nach 120.min)"), y="Proteinexpression", x="Genexpression")

    ggsave(tmpPlot, path = args[3], file=paste0("Genexpression (nach ", substring(colnames(mergeddata)[colN],2) , ") vs Proteinexpression (nach 120.min).png"), width = 14, height = 10, units = "cm")
}

# Spalten definieren, die geplottet werden sollen
coloums <- c(2, 3, 4, 5, 6, 7, 8)

# Funktion drawPlot mit alle Elementen aus coloums ausführen
lapply(coloums, drawPlot)
